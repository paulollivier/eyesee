use rand::Rng;
use std::{
    collections::HashMap,
    io,
    io::Write,
    process::{Command, Output, Stdio},
};
use tracing::{instrument, trace};

pub const DEFAULT_WORK_DIR: &str = "/wd";

#[cfg(test)]
mod tests {
    use crate::utils::docker::{
        generate_docker_name,
        init_container,
        remove_container,
        run_in_container,
        run_in_sh,
        stop_container,
        DockerEnv,
        DockerVolumes,
        DEFAULT_WORK_DIR,
    };
    use test_case::test_case;
    use tracing_test::traced_test;
    #[traced_test]
    #[test_case(true, "echo 'Hello World!'"; "test interactive")]
    #[test_case(false, "echo 'Hello World!'"; "test non-interactive")]
    fn test_docker_run_noninteractive(interactive: bool, prgrm: &str) {
        let container_name = generate_docker_name("eyelib-tests-run-");
        let r = run_in_sh(
            "busybox",
            &container_name,
            DockerVolumes::new(),
            DockerEnv::new(),
            prgrm,
            DEFAULT_WORK_DIR,
            interactive,
        );
        let _ = stop_container(&container_name);
        let _ = remove_container(&container_name);
        assert!(r.is_ok());
        let r = r.unwrap();
        assert!(r.status.success());
    }
    #[traced_test]
    #[test]
    fn test_run_in_container() {
        let container_name = generate_docker_name("eyelib-tests-run-in-container-");
        let r = init_container(
            "busybox",
            &container_name,
            DockerVolumes::new(),
            DockerEnv::new(),
        );
        let r2 = run_in_container(&container_name, "echo 'Hello World!'");
        let _ = stop_container(&container_name);
        let _ = remove_container(&container_name);
        assert!(r.is_ok());
        assert!(r2.is_ok());
        let r2 = r2.unwrap();
        let stdout = String::from_utf8(r2.stdout).expect("Could not convert from utf-8");
        stdout
            .find(r"Hello World!")
            .expect("could not find target string!");
    }
}
const DOCKER_NAME_CHARSET: &[u8] = b"abcdefghijklmnopqrstuvwxyz-_0123456789";
const FORBIDDEN_START_CHARS: &[char] = &['-', '_'];

pub type DockerVolumes = HashMap<String, String>;
pub type DockerEnv = HashMap<String, String>;

pub fn init_container(
    image: &str,
    container_name: &str,
    volumes: DockerVolumes,
    env: DockerEnv,
) -> io::Result<()> {
    run_in_sh(
        image,
        container_name,
        volumes,
        env,
        "exit",
        DEFAULT_WORK_DIR,
        true,
    )?;
    Ok(())
}
#[instrument(ret)]
pub fn generate_docker_name(prefix: &str) -> String {
    let mut rng = rand::thread_rng();
    let s = format!(
        "{}{}",
        prefix,
        (0..16)
            .map(|_| { DOCKER_NAME_CHARSET[rng.gen_range(0..DOCKER_NAME_CHARSET.len())] as char })
            .collect::<String>()
    );
    if s.starts_with(FORBIDDEN_START_CHARS) || s.ends_with(FORBIDDEN_START_CHARS) {
        return generate_docker_name(prefix);
    }
    s
}
#[instrument(skip(volumes, env, interactive), ret)]
pub fn run_in_sh(
    image: &str,
    container_name: &str,
    volumes: DockerVolumes,
    env: DockerEnv,
    program: &str,
    workdir: &str,
    interactive: bool,
) -> io::Result<Output> {
    let mut args = Vec::new();
    args.push("run".to_string());
    if interactive {
        args.push("--interactive".to_string());
    }
    args.push(format!("--name={}", container_name));
    args.extend(volumes.iter().map(|(k, v)| format!("--volume={}:{}", k, v)));
    args.extend(env.iter().map(|(k, v)| format!("--env=\"{}={}\"", k, v)));
    args.push(format!("--workdir={}", workdir));
    args.push("--pull=always".to_string());
    args.push(image.to_string());
    args.push("sh".to_string());
    trace!("running docker {}", &args.join(" "));
    let mut cmd = Command::new("docker")
        .args(args)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()?;
    let stdin = cmd.stdin.as_mut().expect("Could not get child stdin");
    stdin.write_all(program.as_bytes())?;
    cmd.wait_with_output()
}

#[instrument(ret)]
pub fn run_in_container(container_name: &str, program: &str) -> io::Result<Output> {
    let mut cmd = Command::new("docker")
        .args(&["start", "-ai", container_name])
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()?;
    cmd.stdin
        .take()
        .expect("could not obtain handle on stdin")
        .write_all(program.as_bytes())?;
    cmd.wait_with_output()
}

#[instrument(ret)]
pub fn stop_container(container_name: &str) -> io::Result<Output> {
    Command::new("docker")
        .args(&["stop", container_name])
        .output()
}

#[instrument(ret)]
pub fn remove_container(container_name: &str) -> io::Result<Output> {
    Command::new("docker")
        .args(&["rm", container_name])
        .output()
}
