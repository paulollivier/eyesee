use std::{
    io,
    path::Path,
    process::{Command, Output, Stdio},
};
use tracing::instrument;
use url::Url;

#[instrument(ret, skip(url), fields(url=url.to_string()))]
pub(crate) fn clone(dir: &Path, url: &Url) -> io::Result<Output> {
    Command::new("git")
        .env("LANG", "en_US.utf-8")
        .args(&["clone", url.as_ref(), "."])
        .current_dir(dir)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .output()
}
