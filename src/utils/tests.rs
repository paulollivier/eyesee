use std::{
    borrow::BorrowMut,
    env::{current_dir, set_current_dir},
    fmt::{Debug, Formatter},
    io::Write,
    process::{Command, Stdio},
};

use assert_fs::{
    fixture::{FileTouch, PathChild},
    TempDir,
};
use eyelib::{Executor, Job, JobResult};
use rand::Rng;
use s3::creds::Credentials;
use tracing::{debug, info, instrument};

use crate::{
    executors::{
        get_executor,
        local::{ObjectStorageConfig, RegionConfig},
        ExecutorConfig,
    },
    utils::docker,
};

#[cfg(test)]
mod tests {
    use std::{fs::File, io::Write};

    use assert_fs::fixture::PathChild;
    use pretty_assertions::assert_eq;
    use s3::Bucket;
    use tokio;
    use tracing::trace;
    use tracing_test::traced_test;

    use crate::{executors::local::ObjectStorageConfig, utils::tests::ExecutorBuilder};

    #[traced_test]
    #[tokio::test]
    #[allow(unreachable_patterns)]
    async fn test_minio_upload() {
        let builder = ExecutorBuilder::new_local_executor().launch_minio("minio-tests");
        if let Some(conf) = &builder.minio_config {
            match conf {
                ObjectStorageConfig::S3 {
                    bucket_name,
                    region,
                    credentials,
                } => {
                    trace!("creating bucket instance to {}", &bucket_name);
                    let bucket =
                        Bucket::new(bucket_name, region.clone().into(), credentials.clone())
                            .unwrap()
                            .with_path_style();
                    trace!("trying to get head object…");
                    // let (_, code) = bucket
                    //     .head_object("/")
                    //     .await
                    //     .expect("could not fetch head object");
                    // assert_eq!(code, 200);
                    trace!("creating dummy file…");
                    let test: Vec<u8> = (0..1024).map(|_| 42).collect();
                    let path = builder.temp_dir.child("test.bin");
                    let mut file = File::create(&path).unwrap();
                    file.write_all(&test).expect("could not write temp file");
                    let mut file = tokio::fs::File::open(&path)
                        .await
                        .expect("could not open temp file for reading");
                    trace!("attempting to upload it…");
                    let status_code = bucket
                        .put_object_stream(&mut file, "/test.bin")
                        .await
                        .expect("could not upload file");
                    assert_eq!(status_code, 200);
                }
                _ => panic!("invalid variant"),
            }
        } else {
            panic!("invalid config! {:?}", builder.minio_config);
        }
    }
    #[traced_test]
    #[test]
    fn test_multiple_minio() {
        let _b1 = ExecutorBuilder::new_local_executor().launch_minio("minio-tests");
        let _b2 = ExecutorBuilder::new_local_executor().launch_minio("minio-tests");
        println!("the two have launched without issues");
    }
}

pub(crate) struct ExecutorBuilder {
    pub(crate) temp_dir: TempDir,
    executor_config: ExecutorConfig,
    executor: Box<dyn Executor>,
    minio_container_name: Option<String>,
    minio_config: Option<ObjectStorageConfig>,
}
impl Debug for ExecutorBuilder {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "ExecutorBuilder({})", self.temp_dir.path().display())
    }
}

impl Drop for ExecutorBuilder {
    fn drop(&mut self) {
        if let Some(cname) = &self.minio_container_name {
            docker::stop_container(cname).expect("could not stop minio");
            docker::remove_container(cname).expect("could not remove minio");
        }
    }
}

impl ExecutorBuilder {
    pub(crate) fn new_local_executor() -> Self {
        let config_str = r#"---
!local
privileged: false
coordinator_url: http://localhost:7474/
external_url: http://localhost:7575/
storage:
  bucket_name: eyesee-job-workspaces
  credentials:
    access_key: testuser
    secret_key: deadbeef
  region:
    region: minio-tests
    endpoint: http://localhost:9041
"#;
        let conf: ExecutorConfig = serde_yaml::from_str(config_str).unwrap();
        let executor = get_executor(conf.clone()).unwrap();

        Self {
            temp_dir: TempDir::new().unwrap(),
            executor_config: conf,
            executor,
            minio_container_name: None,
            minio_config: None,
        }
    }

    #[instrument(skip_all)]
    pub fn launch_minio(mut self, bucket_prefix: &str) -> Self {
        let port = rand::thread_rng().gen_range(1024..50000);
        let admin_port = rand::thread_rng().gen_range(1024..50000);
        let bucket_name = docker::generate_docker_name(bucket_prefix).replace('_', "-");
        let config = ObjectStorageConfig::S3 {
            bucket_name: bucket_name.clone(),
            region: RegionConfig::Custom {
                region: "tests".to_string(),
                endpoint: format!("http://localhost:{port}"),
            },
            credentials: Credentials {
                access_key: Some("testuser".to_string()),
                secret_key: Some("deadbeef".to_string()),
                security_token: None,
                session_token: None,
                expiration: None,
            },
        };
        let minio_container_name = docker::generate_docker_name("eyesee-tests-minio-");
        let mc_container_name = docker::generate_docker_name("eyesee-tests-mc-");
        info!(container_name = minio_container_name, "launching minio…");
        let r = Command::new("docker")
            .args(&[
                "run",
                "-d",
                "-p",
                &format!("{port}:9000"),
                "-p",
                &format!("{admin_port}:9001"),
                "-e",
                "MINIO_ROOT_USER=testuser",
                "-e",
                "MINIO_ROOT_PASSWORD=deadbeef",
                &format!("--name={}", &minio_container_name),
                "minio/minio",
                "server",
                "/data",
                "--console-address",
                &format!(":{admin_port}"),
            ])
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .output()
            .expect("Could not launch minio for s3 emulation");
        debug!("minio launch output: {}: {:?}", r.status.success(), r);
        assert!(r.status.success()); // this should not fail
        self.minio_container_name = Some(minio_container_name.clone());
        debug!(bucket = &bucket_name, "trying to create bucket…");
        let mut cmd = Command::new("docker")
            .args(&[
                "run",
                "-i",
                "--rm",
                &format!("--name={mc_container_name}"),
                &format!("--link={minio_container_name}:minio"),
                "--entrypoint",
                "/bin/sh",
                "minio/mc",
            ])
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()
            .expect("could not launch mc");
        if let Some(mut stdin) = cmd.stdin.take() {
            stdin.write_all(format!("/usr/bin/mc alias set minio http://minio:9000 testuser deadbeef && /usr/bin/mc mb minio/{bucket_name} && /usr/bin/mc policy set public minio/{bucket_name}; exit 0", bucket_name=&bucket_name).as_bytes()).expect("could not write to stdin");
        }
        // && /usr/bin/mc policy set public minio/{bucket_name}
        let r = cmd.wait_with_output().expect("could not wait with output");
        debug!(
            "Finished trying to create bucket: {}: {:?}",
            r.status.success(),
            r
        );
        assert!(r.status.success());
        // let r = docker::run_in_sh("minio/mc", &mc_container_name, DockerVolumes::new(), DockerEnv::new(), &format!("/usr/bin/mc alias set minio http://localhost:{port} test-user deadbeef && /usr/bin/mc mb minio/{bucket_name} && /usr/bin/mc policy set public minio/{bucket_name}", bucket_name=&bucket_name), "/", false).expect("Could not create bucket");
        // assert!(r.status.success());
        info!("finished bucket creation");
        self.minio_config = Some(config.clone());
        match self.executor_config.borrow_mut() {
            ExecutorConfig::Local(executor_config) => {
                executor_config.storage = config;
            }
        }
        self.executor = get_executor(self.executor_config.clone()).unwrap();
        self
    }
    #[instrument]
    pub(crate) fn generate_empty_git_dir(self, repo_name: &str) -> Self {
        const FNAME: &str = ".gitkeep";
        let old_cwd = current_dir().expect("could not get cwd");
        debug!("{}", current_dir().unwrap().display());
        set_current_dir(self.temp_dir.path()).expect("Could not set cwd");
        debug!("{}", current_dir().unwrap().display());
        debug!("creating git repo {}", repo_name);
        let res = Command::new("git")
            .args(&["init", repo_name])
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .status()
            .expect("Could not run git");
        assert!(res.success());
        debug!("creating file {} in repo", FNAME);
        self.temp_dir
            .child(repo_name)
            .child(FNAME)
            .touch()
            .expect("Could not create file");
        debug!("cd'ing into repo");
        set_current_dir(self.temp_dir.path().join(repo_name)).expect("Could not set cwd");
        debug!("{}", current_dir().unwrap().display());
        debug!("git add {}", FNAME);
        let res = Command::new("git")
            .args(&["add", FNAME])
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .status()
            .expect("could not run git");
        assert!(res.success());
        debug!("creating initial commit");
        let res = Command::new("git")
            .args(&["commit", "-m", r#""Initial Commit""#])
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .status()
            .expect("Could not run git");
        assert!(res.success());
        set_current_dir(old_cwd).expect("could not go back to old cwd");
        debug!("{}", current_dir().unwrap().display());
        return self;
    }
    #[instrument(skip(job, tests))]
    pub(crate) async fn execute(self, job: Job, tests: fn(eyelib::Result<JobResult>)) -> Self {
        debug!("Running the given job…");
        let res = self.executor.execute(job).await;
        debug!("Running given tests…");
        tests(res);
        return self;
    }
}
