//! All executor modules must implement a `pub fn new(config: module::Config) ->
//! Self`
use eyelib::{Executor, Result};
use serde::Deserialize;

/// A simple local docker executor. Calls the host's docker command to run the
/// jobs.
pub mod local;

/// All possible executors config variants
#[derive(Deserialize, Clone)]
#[serde(rename_all = "lowercase")]
pub enum ExecutorConfig {
    /// A local docker executor's configuration
    Local(local::Config),
}

/// Returns an executor matching the type of the config
pub fn get_executor(config: ExecutorConfig) -> Result<Box<dyn Executor>> {
    match config {
        ExecutorConfig::Local(conf) => Ok(Box::new(local::new(conf)?)),
    }
}

#[cfg(test)]
mod tests {
    use crate::utils::tests::ExecutorBuilder;
    use tracing_test::traced_test;
    #[traced_test]
    #[test]
    fn test_get_executor_simple_local() {
        ExecutorBuilder::new_local_executor();
    }
}
