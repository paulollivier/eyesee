use chrono::Utc;
use serde::Deserialize;
use std::{
    env,
    fs,
    io,
    path::Path,
    process::{Command, Output, Stdio},
    str::FromStr,
};

use crate::utils::{
    docker,
    docker::{DockerEnv, DockerVolumes, DEFAULT_WORK_DIR},
    git,
};
use async_trait::async_trait;
use eyelib::{Executor, ExecutorError, Job, JobResult, JobWorkspace, Result, StepResult};
use s3::{creds::Credentials, Bucket, Region};
use tempdir::TempDir;
use tracing::{debug, event, info, instrument, trace, Level};
use url::Url;
use uuid::Uuid;

#[derive(Deserialize, Debug, Clone)]
#[serde(untagged)]
pub enum RegionConfig {
    Custom { region: String, endpoint: String },
    Vanilla { name: String },
}

impl From<RegionConfig> for Region {
    fn from(region_config: RegionConfig) -> Self {
        match region_config {
            RegionConfig::Custom { region, endpoint } => Region::Custom { region, endpoint },
            RegionConfig::Vanilla { name } => name.parse().unwrap(),
        }
    }
}

#[derive(Deserialize, Debug, Clone)]
#[serde(untagged)]
/// Configuration representing the object storage in use to store job workspaces
pub enum ObjectStorageConfig {
    S3 {
        bucket_name: String,
        region: RegionConfig,
        credentials: Credentials,
    },
}

impl ObjectStorageConfig {
    #[instrument(ret)]
    pub async fn upload(&self, file_path: &Path) -> io::Result<Url> {
        match self {
            ObjectStorageConfig::S3 {
                bucket_name,
                region,
                credentials,
            } => {
                let mut s3_bucket = Bucket::new(
                    bucket_name,
                    match region {
                        RegionConfig::Vanilla { name } => name.parse().unwrap(),
                        RegionConfig::Custom {
                            region: name,
                            endpoint,
                        } => Region::Custom {
                            region: name.clone(),
                            endpoint: endpoint.clone(),
                        },
                    },
                    credentials.clone(),
                )
                .unwrap();
                // needed for minio tests
                if let RegionConfig::Custom { .. } = region {
                    s3_bucket = s3_bucket.with_path_style();
                }
                let mut f = tokio::fs::File::open(file_path).await?;
                let s3_file_path =
                    Path::new("/").join(file_path.file_name().unwrap().to_str().unwrap());
                trace!(
                    "uploading file {} to {}…",
                    file_path.display(),
                    s3_file_path.display()
                );
                s3_bucket
                    .put_object_stream(&mut f, &s3_file_path.display().to_string())
                    .await
                    .unwrap();
                Ok(
                    Url::from_str(&format!("{}/{}", s3_bucket.url(), &s3_file_path.display()))
                        .unwrap(),
                )
            }
        }
    }
}

#[derive(Deserialize, Clone)]
/// Configuration for the [`LocalDockerExecutor`]
pub struct LocalDockerExecutorConfig {
    /// Should we run docker with --privileged=true ?
    pub privileged: bool,
    /// Where can we find our coordinator?
    pub coordinator_url: Url,
    /// Where the coordinator can find us
    pub external_url: Url,
    /// Object Storage config
    pub storage: ObjectStorageConfig,
}

pub(crate) type Config = LocalDockerExecutorConfig;

/// A quite simple executor, calling via shell the host's `docker` command.
pub struct LocalDockerExecutor {
    config: Config,
    is_busy: bool,
}

impl LocalDockerExecutor {
    /// Gets Workspace & sets it up in the given `dir`
    #[instrument(skip(self, workspace), ret)]
    pub(crate) async fn set_workspace(
        &self,
        dir: &Path,
        workspace: &JobWorkspace,
    ) -> Result<Output> {
        match workspace {
            JobWorkspace::Git(url) => {
                debug!("Detected a git workspace");
                debug!("Cloning {} in {}/…", url, dir.display());
                Ok(git::clone(dir, url)?)
            }
            JobWorkspace::TarGz(_url) => {
                debug!("Detected a TarGz workspace");
                todo!();
            }
        }
    }
    #[instrument(skip(self), ret)]
    pub async fn save_workspace(&self, dir: &Path, job_id: Uuid) -> Result<JobWorkspace> {
        let tar_name = format!("{}.tgz", job_id);
        let archive_path = env::temp_dir().join(&tar_name);
        debug!(
            archive_path = archive_path.display().to_string(),
            "creating archive…"
        );
        let output = Command::new("tar")
            .args(&[
                "-zcf",
                &archive_path.display().to_string(),
                "-C",
                &dir.display().to_string(),
                ".",
            ])
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .output()?;
        if !output.status.success() {
            return Err(ExecutorError::InternalError {
                msg: "Could not compress workspace.".to_string(),
            });
        }
        let url = self.config.storage.upload(&archive_path).await?;
        fs::remove_file(&archive_path)?;
        Ok(JobWorkspace::TarGz(url))
    }
}

/// Returns a new [`LocalDockerExecutor`]
pub fn new(config: Config) -> Result<LocalDockerExecutor> {
    Ok(LocalDockerExecutor {
        config,
        is_busy: false,
    })
}

#[async_trait]
impl Executor for LocalDockerExecutor {
    #[instrument(skip_all, fields(job_id=job.id.to_string(), job_name=job.job_name))]
    async fn execute(&self, job: Job) -> Result<JobResult> {
        let temp_dir = TempDir::new("EyeSee")?;
        let mut job_result = JobResult::from(&job);
        self.set_workspace(temp_dir.path(), &job.workspace).await?;
        let volumes = DockerVolumes::from([(
            temp_dir.path().display().to_string(),
            DEFAULT_WORK_DIR.to_string(),
        )]);
        let container_name =
            docker::generate_docker_name(&format!("eye-{}", &job.job_name.replace(' ', "-")));
        info!("Running in container {}", container_name);
        docker::init_container(&job.image, &container_name, volumes, DockerEnv::new())?;
        for step in &job.steps {
            let mut step_res = StepResult {
                step: step.clone(),
                logs: vec![],
                started_at: Utc::now(),
                finished_at: Default::default(),
            };
            let output = docker::run_in_container(&container_name, step)?;
            let stdout = String::from_utf8_lossy(&output.stdout);
            event!(
                Level::DEBUG,
                stdout = stdout.to_string(),
                "docker run finished"
            );
            for line in stdout.lines() {
                step_res.logs.push((Utc::now(), line.to_string()))
            }
            step_res.finished_at = Utc::now();
            job_result.steps_results.push(step_res);
        }
        job_result.workspace = self.save_workspace(temp_dir.path(), job.id).await?;
        docker::stop_container(&container_name)?;
        docker::remove_container(&container_name)?;
        Ok(job_result)
    }
    async fn can_execute(&self) -> bool {
        self.is_busy
    }
    #[instrument(skip_all)]
    async fn register(self, _coordinator_url: Url) -> Result<()> {
        todo!()
    }
    #[instrument(skip_all)]
    async fn deregister(self, _coordinator_url: Url) -> Result<()> {
        todo!()
    }
}

#[cfg(test)]
mod tests {
    use crate::utils::tests::ExecutorBuilder;
    use eyelib::{Job, JobWorkspace, JobWorkspace::Git};
    use std::str::FromStr;
    use tracing_test::traced_test;
    use url::Url;
    use uuid::Uuid;

    #[traced_test]
    #[tokio::test]
    async fn test_simple_docker_hello_world() {
        let executor = ExecutorBuilder::new_local_executor();
        let git_repo_path = format!(
            "file://{}",
            executor.temp_dir.path().join("empty").display()
        );
        executor
            .launch_minio("test-workspaces-storage")
            .generate_empty_git_dir("empty")
            .execute(
                Job {
                    job_name: "say hello".to_string(),
                    id: Default::default(),
                    image: "busybox".to_string(),
                    steps: vec!["echo 'Hello, World!'".to_string()],
                    workspace: Git(Url::from_str(&git_repo_path).unwrap()),
                },
                |res| {
                    assert!(res.is_ok());
                    let res = res.unwrap();
                    assert_eq!(res.id, Uuid::default());
                    assert_eq!(res.steps_results.len(), 1);
                    let step_res = &res.steps_results[0];
                    assert_eq!(step_res.step, "echo 'Hello, World!'");
                    assert!(step_res.logs.len() >= 1);
                    match res.workspace {
                        Git(_) => {
                            panic!("We were supposed to get a TarGz workspace!")
                        }
                        JobWorkspace::TarGz(_url) => {}
                    }
                },
            )
            .await;
    }
}
